## Roadmap

- [ X ] Basic CRUD implementation on properties.
- [ ] Basic CRUD implementation on Optional properties.
- [ ] Implementation of Roles (`Issuer`, `Holder`, `Verifier`, etc.).
- [ ] Connecting to `Verifiable Data Registry`.
- [ ] Validation for all properties.
- [ ] Verifying basic cryptographic proofs.
- [ ] Implementation of Zero-Knowledge proof.
