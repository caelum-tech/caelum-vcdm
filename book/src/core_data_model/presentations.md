# Presentations
 >  The expression of a subset of one's persona is called a *verifiable presentation*. Examples of different 
personas include a person's professional persona, their online gaming persona, their family persona, or an 
incognito persona.

>A verifiable presentation expresses data from one or more verifiable credentials, and is packaged in such a 
way that the authorship of the data is verifiable.

>The data in a presentation is often about the same subject, but might have been issued by multiple issuers. 
The aggregation of this information typically expresses an aspect of a person, organization, or entity.

![](https://www.w3.org/TR/vc-data-model/diagrams/presentation.svg)

A more complete version of a *verifiable presentation*  is contained in the following figure:

![](https://www.w3.org/TR/vc-data-model/diagrams/presentation-graph.svg)