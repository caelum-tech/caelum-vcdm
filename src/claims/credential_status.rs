use serde::{Deserialize, Serialize};
use wasm_bindgen::prelude::*;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct CredentialStatus {
    pub id: String,
    #[serde(rename(serialize = "type", deserialize = "type"))]
    pub credential_status_type: String,
    #[serde(rename(serialize = "currentStatus", deserialize = "currentStatus"))]
    pub current_status: String,
    #[serde(rename(serialize = "statusReason", deserialize = "statusReason"))]
    pub status_reason: String,
}

impl Default for CredentialStatus {
    fn default() -> Self {
        CredentialStatus {
            id: "".to_string(),
            credential_status_type: "".to_string(),
            current_status: "".to_string(),
            status_reason: "".to_string(),
        }
    }
}

impl CredentialStatus {
    pub fn from_json(json: JsValue) -> Self {
        json.into_serde().unwrap()
    }

    pub fn to_str(&self) -> String {
        serde_json::to_string(&self).unwrap()
    }

    pub fn new(
        id: String,
        status_type: String,
        current_status: String,
        status_reason: String,
    ) -> Self {
        CredentialStatus {
            id,
            credential_status_type: status_type,
            current_status,
            status_reason,
        }
    }
}
