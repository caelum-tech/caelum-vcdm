extern crate hex;
//use base64::{encode, decode};
//use ed25519_dalek::{ PublicKey, Signature, SignatureError, PUBLIC_KEY_LENGTH };
use sha2::{Digest, Sha512};

pub fn get_utc_time() -> String {
    #[cfg(target_arch = "wasm32")]
    return stdweb::web::Date::from_time(stdweb::web::Date::now()).to_iso_string(); // e.g. `2014-11-28T12:45:59.324310806Z`

    #[cfg(not(target_arch = "wasm32"))]
    {
        use chrono::Utc;
        Utc::now().to_rfc3339() + "Z" // e.g. `2014-11-28T12:45:59.324310806Z`
    }
}

pub fn create_hash(string: &str) -> String {
    let mut hash: Sha512 = Sha512::new();
    hash.input(string);
    let hash_vec = hash.result();
    hex::encode(hash_vec)
}

pub fn create_u8a_hash(string: &str) -> Vec<u8> {
    let mut hash: Sha512 = Sha512::new();
    hash.input(string);
    hash.result().to_vec()
}

pub fn hex_to_u8a(hexa: &str) -> Vec<u8> {
    hexa.as_bytes()
        .chunks(2)
        .map(std::str::from_utf8)
        .collect::<Result<Vec<&str>, _>>()
        .unwrap()
        .into_iter()
        .map(|h| u8::from_str_radix(h, 16).unwrap())
        .collect()
}

pub fn u8a_to_hex(vec: &[u8]) -> String {
    hex::encode(vec).to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_create_hash() {
        let should = "31bca02094eb78126a517b206a88c73cfa9ec6f704c7030d18212cace820f025f00bf0ea68dbf3f3a5436ca63b53bf7bf80ad8d5de7d8359d0b7fed9dbc3ab99";
        assert_eq!(create_hash("0"), should.to_string());
    }

    #[test]
    fn test_create_u8a_hash() {
        let should = vec![
            49, 188, 160, 32, 148, 235, 120, 18, 106, 81, 123, 32, 106, 136, 199, 60, 250, 158,
            198, 247, 4, 199, 3, 13, 24, 33, 44, 172, 232, 32, 240, 37, 240, 11, 240, 234, 104,
            219, 243, 243, 165, 67, 108, 166, 59, 83, 191, 123, 248, 10, 216, 213, 222, 125, 131,
            89, 208, 183, 254, 217, 219, 195, 171, 153,
        ];
        assert_eq!(create_u8a_hash("0"), should);
    }

    #[test]
    fn test_hex_to_u8a() {
        let should = vec![
            49, 188, 160, 32, 148, 235, 120, 18, 106, 81, 123, 32, 106, 136, 199, 60, 250, 158,
            198, 247, 4, 199, 3, 13, 24, 33, 44, 172, 232, 32, 240, 37, 240, 11, 240, 234, 104,
            219, 243, 243, 165, 67, 108, 166, 59, 83, 191, 123, 248, 10, 216, 213, 222, 125, 131,
            89, 208, 183, 254, 217, 219, 195, 171, 153,
        ];
        let zero = "31bca02094eb78126a517b206a88c73cfa9ec6f704c7030d18212cace820f025f00bf0ea68dbf3f3a5436ca63b53bf7bf80ad8d5de7d8359d0b7fed9dbc3ab99";
        let au8 = hex_to_u8a(zero);
        assert_eq!(au8, should);
    }

    #[test]
    fn test_u8a_to_hex() {
        let should = "31bca02094eb78126a517b206a88c73cfa9ec6f704c7030d18212cace820f025f00bf0ea68dbf3f3a5436ca63b53bf7bf80ad8d5de7d8359d0b7fed9dbc3ab99";
        let zero = vec![
            49, 188, 160, 32, 148, 235, 120, 18, 106, 81, 123, 32, 106, 136, 199, 60, 250, 158,
            198, 247, 4, 199, 3, 13, 24, 33, 44, 172, 232, 32, 240, 37, 240, 11, 240, 234, 104,
            219, 243, 243, 165, 67, 108, 166, 59, 83, 191, 123, 248, 10, 216, 213, 222, 125, 131,
            89, 208, 183, 254, 217, 219, 195, 171, 153,
        ];
        let au8 = u8a_to_hex(&zero);
        assert_eq!(au8, should);
    }

    #[test]
    fn test_get_utc_time() {
        let time = get_utc_time();
        println!("Timestamp RFC3339 or ISO8601 Zulu Time: {}", time);
    }

    #[test]
    fn test_architecture_guard_headers() {
        #[cfg(not(target_os = "x86_64"))]
        println!("Yes. It's definitely linux!");

        #[cfg(target_os = "wasm32")]
        println!("Yes. It's definitely WebAssembly!");

        println!("test_architecture_guard_headers ends here!");
    }
}
