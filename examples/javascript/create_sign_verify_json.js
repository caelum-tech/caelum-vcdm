let vp_json = {
    "@context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://www.w3.org/2018/credentials/examples/v1",
        "added Context"
    ],
    "id": "did:example:ebfeb1276e12ec21f712ebc6f1c",
    "type": [
        "VerifiableCredential",
        "PersonalInformation",
        "added type"
    ],
    "verifiableCredential": [
        {
             "@context": [
                 "https://www.w3.org/2018/credentials/v1",
                 "https://www.w3.org/2018/credentials/examples/v1"
             ],
             "type": ["VerifiableCredential"],
             "id": "my_id",
             "issuer": "Issuer",
             "claims": [{
                "id": "my_id",
                "credentialStatus": {
                    "id": "my_id",
                    "type": "EmailCredential"
                },
                "credentialSubject": [
                    {"email": "foo@bar.com"}
                ],
                "issuer": "did:example:issuer",
                "issuanceDate": "2018-06-17T10:03:48Z",
                "type": ["EmailCredential", "VerifiableCredential"]
             }],
             "proof": [{
                "type": "RsaSignature2018",
                "created": "2018-06-17T10:03:48Z",
                "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
                "signatureValue": "pY9...Cky6Ed = "
             }],
             "nonRevocationProof": [{
                 "type": "",
                 "created": "",
                 "verificationMethod": "",
                 "signatureValue": ""
             }]
         }
    ],
    "proof": {
        "type": "RsaSignature2018",
        "created": "2018-06-17T10:03:48Z",
        "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
        "signatureValue": "pY9...Cky6Ed = "
    }
};

let vp = vcdm.VPresentation.fromJSON(vp_json);

console.log(vp.toJSON());