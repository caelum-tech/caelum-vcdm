# Summary

- [Verifiable Credentials Data Model](core_data_model/description.md)
    - [Key Concepts](core_data_model/key_concepts.md)
    - [Claims](core_data_model/claims.md)
    - [Credentials](core_data_model/credentials.md)
    - [Presentations](core_data_model/presentations.md)
- [Getting started with JS](getting_started_js.md)
- [Getting started with Rust](getting_started_rust.md)
- [Properties](./properties/properties.md)
    - [Context](properties/contexts.md)
    - [Identifiers](./properties/identifiers.md)
    - [Types](./properties/types.md)
    - [Credential Subjects](./properties/credential_subject.md)
    - [Issuer](./properties/issuer.md)
    - [Issuance Date](./properties/issuance_date.md)
    - [Proof](./properties/proof.md)
    - [Expiration](./properties/expiration.md)
    - [Status](./properties/status.md)
    - [Presentations](./properties/presentations.md)
- [Defining Roles](./roles.md)
- [Connecting to Verifiable Data Registry](./data_registry.md)
- [Roadmap](./roadmap.md)
- [Contributing](./contributing.md)
- [Acknowledgement](./acknowledgement.md)


        