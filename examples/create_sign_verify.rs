extern crate caelum_vcdm;

use caelum_vcdm::claims::{verify_claim, Claim};
use caelum_vcdm::proofs::proof::Proof;
use caelum_vcdm::verifiable_credential::VerifiableCredential;

/*
* Creates a new Verifiable Credential by W3C
* https://www.w3.org/TR/vc-data-model/
*/
fn create_credential() -> VerifiableCredential {
    // Creates a new Verifiable Credential
    let mut ver_cred_vcdm =
        VerifiableCredential::new("http://example.com/credentials/4643".to_string());
    ver_cred_vcdm.proofs.push(
        serde_json::from_str(
            r#"{
            "type": "RsaSignature2018",
            "created": "2018-06-17T10:03:48Z",
            "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
            "signatureValue": "pY9...Cky6Ed = "
        }"#,
        )
        .unwrap(),
    );

    return ver_cred_vcdm;
}

fn create_claim() -> Claim {
    // Creates Claim
    return serde_json::from_str(
        r#"
        {"credentialStatus":{"id":"https://lorena.caelumlabs.com/credentials",
        "type":"CaelumEmailCredential","currentStatus":"pending","statusReason":"self-issued"},
        "type":["VerifiableCredential","CaelumEmailCredential"],
        "credentialSubject":[{"email":"asd","last":"","type":"CaelumEmailCredential","first":""}],
        "id":"Hash: 0xa76745d559991b293036244416c6818b15d8debf6960151d4c6abf5b284fbb1b",
        "issuanceDate":"2019-10-16T14:07:41.657Z",
        "issuer":"did:lrn:caelum-email-verifier:ebfeb1276e12ec21f712ebc6f1c#k2"}"#,
    )
    .unwrap();
}

fn create_proof(keys: &[u8], c: &Claim) -> Proof {
    // Creates Proof from claim (signature is created automatically)
    return Proof::from_claim_and_keys(&c, &keys);
}

/*
* Checking the presented item against the core data model, and may also include validating the provided proof section and checking the item's status.
* Is it a valid claim?
*/
fn is_valid_claim(v: VerifiableCredential) -> bool {
    if v.claims.last().is_some() && v.proofs.last().is_some() {
        let c = v.claims.last().unwrap();
        let p = v.proofs.last().unwrap();

        // Verify signature from proof & claim
        assert_eq!(verify_claim(&c, &p), Ok(()));

        return c.verify(&p);
    } else {
        return false;
    }
}

fn main() {
    // Signature from claim
    let keys = vec![
        212, 109, 1, 120, 214, 102, 78, 169, 141, 239, 187, 76, 224, 61, 74, 250, 20, 4, 89, 89,
        159, 113, 116, 168, 87, 79, 196, 25, 155, 87, 180, 134, 65, 189, 48, 58, 3, 235, 0, 194,
        39, 238, 5, 140, 57, 222, 169, 56, 62, 89, 169, 66, 225, 253, 97, 6, 241, 72, 72, 72, 195,
        209, 12, 81,
    ];

    let mut example_credential = create_credential();
    let example_claim = create_claim();
    let example_proof = create_proof(&keys, &example_claim);

    example_credential.claims.push(example_claim);
    example_credential.proofs.push(example_proof);

    // Verify signature from claim
    if is_valid_claim(example_credential) {
        println!("Claim is valid!");
    } else {
        println!("Claim is invalid!");
    }
}
