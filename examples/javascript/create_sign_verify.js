// Constructing a Claim
let claim = new vcdm.VClaim()
    .setCredentialStatus({id: "my_id", type: "EmailCredential"})
    .setCredentialType(["EmailCredential", "VerifiableCredential"])
    .setCredentialSubject({email: "foo@bar.com"})
    .setId("my_id")
    .setIssuanceDate("2018-06-17T10:03:48Z")
    .setIssuer("did:example:issuer");

console.log(claim.toJSON());

// Contructing a Verifiable Credential
let vc = new vcdm.VCredential("my_id")
    // Setters overwrite values of property
    .setIssuer("Issuer")
    .setClaim(claim.toJSON())
    // Setting another credential totally different and defined by user
    .setProof({
        "type": "RsaSignature2018",
        "created": "2018-06-17T10:03:48Z",
        "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
        "signatureValue": "pY9...Cky6Ed = "
    })
    // Adders push values to the vector properties
    .addContext("added context")
    .addType("added type");

console.log(vc.toJSON());

// Contructing a Verifiable Presentation
let vp = new vcdm.VPresentation("my_id")
    .addContext("added Context")
    .addType("added type")
    // Add the created Verifiable Credential
    .addVerifiableCredential(vc.toJSON())
    .setProof({
        "type": "RsaSignature2018",
        "created": "2018-06-17T10:03:48Z",
        "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
        "signatureValue": "pY9...Cky6Ed = "
    });

console.log(vp.toJSON());

// Signature from claim
let claim = new vcdm.VClaim()
    .setCredentialStatus({id: "my_id", type: "EmailCredential"})
    .setCredentialType(["EmailCredential", "VerifiableCredential"])
    .setCredentialSubject({email: "foo@bar.com"})
    .setId("my_id")
    .setIssuanceDate("2018-06-17T10:03:48Z")
    .setIssuer("did:example:issuer");
const keys = new Uint8Array(64);
const signature = claim.sign(keys)
console.log(signature)

// Proof from claim (signature is created automatically)
const proof = vcdm.VProof.fromClaimAndKeys(claim.toJSON(), keys)

// Verify signature from proof & claim
console.log(
    vcdm.verifyClaim(claim.toJSON(), proof.toJSON())
)

// Verify signature from claim
console.log(
    claim.verify(proof.toJSON())
)