//!
//! # **V**erifiable **C**redential **D**ata **M**odel
//!
//! This crates implements w3's
//! [Verifiable Credential Data Model](https://www.w3.org/TR/verifiable-claims-data-model/)
//! specifications.
//! This means it will create, read, update and delete (a.k.a. CRUD) all properties of a
//! `Verifiable Credential`and a `Verifiable Presentation`. As well as interacting with other
//! `Verifiable Credential` and `Verifiable Presentation`.
//!
//! >A `Verifiable Credential` or a `Verifiable Presentation`  can represent all of the same
//! information that a physical credential represents. The addition of technologies, such as
//! digital signatures, makes verifiable credentials more tamper-evident and more trustworthy
//! than their physical counterparts.
//!
//! > In the physical world, a credential might consist of:
//! > - Information related to identifying the subject of the credential (for example, a photo, name,
//! or identification number)
//! > - Information related to the issuing authority (for example, a city government, national
//! agency, or certification body)
//! > - Information related to the type of credential this is (for example, a Dutch passport, an
//! American driving license, or a health insurance card)
//! > - Information related to specific attributes or properties being asserted by the issuing
//! authority about the subject (for example, nationality, the classes of vehicle entitled to
//! drive, or date of birth)
//! > - Evidence related to how the credential was derived
//! > - Information related to constraints on the credential (for example, expiration date, or terms
//! of use).
//!
//! We want to help developers create these scenarios and making it easy to interact with other
//! following the same specifications.
//!
#![allow(clippy::new_without_default)]
#[cfg(feature = "serde-serialize")]
extern crate serde;

extern crate serde;

extern crate serde_json;
extern crate wasm_bindgen;

use wasm_bindgen::prelude::*;

pub mod claims;
pub mod proofs;
pub mod utils;
pub mod verifiable_credential;
pub mod verifiable_presentation;

use claims::Claim;
use proofs::{non_revocation_proof::NonRevocationProof, proof::Proof};
use verifiable_credential::VerifiableCredential;
use verifiable_presentation::VerifiablePresentation;

///
/// # Utils interface
///
#[wasm_bindgen]
pub fn create_hash(s: String) -> String {
    utils::create_hash(&s)
}

#[wasm_bindgen]
pub fn create_u8a_hash(s: &str) -> Vec<u8> {
    utils::create_u8a_hash(s)
}

#[wasm_bindgen]
pub fn hex_to_u8a(hexa: &str) -> Vec<u8> {
    utils::hex_to_u8a(hexa)
}

#[wasm_bindgen]
pub fn u8a_to_hex(vec: &[u8]) -> String {
    utils::u8a_to_hex(vec)
}

///
/// # WebAssembly gateway for Proofs
///
#[wasm_bindgen]
pub struct VProof {
    ctx: JsValue,
}

#[wasm_bindgen]
impl VProof {
    #[wasm_bindgen(constructor)]
    pub fn new() -> Self {
        let d = Proof::default();
        VProof { ctx: d.to_json() }
    }

    #[wasm_bindgen(js_name = fromJSON)]
    pub fn from_json(json: JsValue) -> Self {
        let vc: Proof = json.into_serde().unwrap();
        VProof { ctx: vc.to_json() }
    }

    #[wasm_bindgen(js_name = fromClaimAndKeys)]
    pub fn from_claim_and_keys(json_claim: JsValue, keys: &[u8]) -> Self {
        let claim = json_claim.into_serde().unwrap();
        let proof = Proof::from_claim_and_keys(&claim, &keys);
        VProof {
            ctx: proof.to_json(),
        }
    }

    #[wasm_bindgen(js_name = toJSON)]
    pub fn to_json(&self) -> JsValue {
        self.ctx.clone()
    }
}

///
/// # WebAssembly gateway for ZenroomProofs
///
#[wasm_bindgen]
pub struct VNonRevocationProof {
    ctx: JsValue,
}

#[wasm_bindgen]
impl VNonRevocationProof {
    #[wasm_bindgen(constructor)]
    pub fn new() -> Self {
        let d = NonRevocationProof::default();
        VNonRevocationProof { ctx: d.to_json() }
    }

    #[wasm_bindgen(js_name = fromJSON)]
    pub fn from_json(json: JsValue) -> Self {
        let vc: NonRevocationProof = json.into_serde().unwrap();
        VNonRevocationProof { ctx: vc.to_json() }
    }

    #[wasm_bindgen(js_name = toJSON)]
    pub fn to_json(&self) -> JsValue {
        self.ctx.clone()
    }
}

///
/// # WebAssembly gateway for Claims
///
#[wasm_bindgen]
pub struct VClaim {
    ctx: JsValue,
}

#[wasm_bindgen]
impl VClaim {
    #[wasm_bindgen(constructor)]
    pub fn new() -> Self {
        let d = Claim::default();
        VClaim { ctx: d.to_json() }
    }

    #[wasm_bindgen(js_name = fromJSON)]
    pub fn from_json(json: JsValue) -> Self {
        let vc: Claim = json.into_serde().unwrap();
        VClaim { ctx: vc.to_json() }
    }

    #[wasm_bindgen(js_name = toJSON)]
    pub fn to_json(&self) -> JsValue {
        self.ctx.clone()
    }

    #[wasm_bindgen(js_name = sign)]
    pub fn sign(&self, keys: &[u8]) -> String {
        let claim: Claim = self.ctx.into_serde().unwrap();
        claim.sign(keys)
    }

    #[wasm_bindgen(js_name = verify)]
    pub fn verify(&self, proof: JsValue) -> bool {
        match claims::verify_claim(
            &self.ctx.into_serde().unwrap(),
            &proof.into_serde().unwrap(),
        ) {
            Ok(_) => true,
            _ => false,
        }
    }

    #[wasm_bindgen(js_name = getCredentialStatus)]
    pub fn get_credential_status(&self) -> JsValue {
        let claim: &Claim = &self.ctx.into_serde().unwrap();
        JsValue::from_serde(&claim.credential_status).unwrap()
    }

    #[wasm_bindgen(js_name = setCredentialStatus)]
    pub fn set_credential_status(self, cs: JsValue) -> Self {
        let mut c: Claim = self.ctx.into_serde().unwrap();
        c.credential_status = cs.into_serde().unwrap();
        VClaim { ctx: c.to_json() }
    }

    #[wasm_bindgen(js_name = getCredentialType)]
    pub fn get_credential_type(&self) -> JsValue {
        let claim: &Claim = &self.ctx.into_serde().unwrap();
        JsValue::from_serde(&claim.credential_type).unwrap()
    }

    #[wasm_bindgen(js_name = setCredentialType)]
    pub fn set_credential_type(self, types: JsValue) -> Self {
        let mut c: Claim = self.ctx.into_serde().unwrap();
        c.credential_type = types.into_serde().unwrap();
        VClaim { ctx: c.to_json() }
    }

    #[wasm_bindgen(js_name = getCredentialSubject)]
    pub fn get_credential_subject(&self) -> JsValue {
        let claim: &Claim = &self.ctx.into_serde().unwrap();
        JsValue::from_serde(&claim.credential_subject).unwrap()
    }

    #[wasm_bindgen(js_name = setCredentialSubject)]
    pub fn set_credential_subject(self, cs: JsValue) -> Self {
        let mut c: Claim = self.ctx.into_serde().unwrap();
        c.credential_subject = cs.into_serde().unwrap();
        VClaim { ctx: c.to_json() }
    }

    #[wasm_bindgen(js_name = getId)]
    pub fn get_id(&self) -> JsValue {
        let claim: &Claim = &self.ctx.into_serde().unwrap();
        JsValue::from_serde(&claim.id).unwrap()
    }

    #[wasm_bindgen(js_name = setId)]
    pub fn set_id(self, id: String) -> Self {
        let mut c: Claim = self.ctx.into_serde().unwrap();
        c.id = id;
        VClaim { ctx: c.to_json() }
    }

    #[wasm_bindgen(js_name = getIssuanceDate)]
    pub fn get_issuance_date(&self) -> JsValue {
        let claim: &Claim = &self.ctx.into_serde().unwrap();
        JsValue::from_serde(&claim.issuance_date).unwrap()
    }

    #[wasm_bindgen(js_name = setIssuanceDate)]
    pub fn set_issuance_date(self, issuance_date: String) -> Self {
        let mut c: Claim = self.ctx.into_serde().unwrap();
        c.issuance_date = issuance_date;
        VClaim { ctx: c.to_json() }
    }

    #[wasm_bindgen(js_name = getIssuer)]
    pub fn get_issuer(&self) -> JsValue {
        let claim: &Claim = &self.ctx.into_serde().unwrap();
        JsValue::from_serde(&claim.issuer).unwrap()
    }

    #[wasm_bindgen(js_name = setIssuer)]
    pub fn set_issuer(self, issuer: String) -> Self {
        let mut c: Claim = self.ctx.into_serde().unwrap();
        c.issuer = issuer;
        VClaim { ctx: c.to_json() }
    }
}

#[wasm_bindgen]
pub fn verify_claim(claim: JsValue, proof: JsValue) -> bool {
    match claims::verify_claim(&claim.into_serde().unwrap(), &proof.into_serde().unwrap()) {
        Ok(_) => true,
        _ => false,
    }
}

///
/// # WebAssembly gateway for Verifiable Credentials
///
#[wasm_bindgen]
pub struct VCredential {
    ctx: JsValue,
}

#[wasm_bindgen]
impl VCredential {
    /// # WebAssembly Constructor
    /// `VCredential`'s constructor will create an empty `VCredential` object. An empty object
    /// is not a valid `VCredential`.
    ///
    #[wasm_bindgen(constructor)]
    pub fn new(id: String) -> Self {
        let d = VerifiableCredential::new(id);
        VCredential { ctx: d.to_json() }
    }

    /// # WebAssembly Constructor `formJSON`
    /// `VCredential`'s constructor will create a `VCredential` object from input. Input must be a
    /// JSON object with all properties defined. If no value is wanted for certain property, must
    /// be empty. This is also true for sub properties (properties of properties).
    ///
    #[wasm_bindgen(js_name = fromJSON)]
    pub fn from_json(json: JsValue) -> Self {
        let vc: VerifiableCredential = json.into_serde().unwrap();
        VCredential { ctx: vc.to_json() }
    }

    // SETTERS (OVERWRITE)
    /// # Set context
    /// By default `context` will be set to
    /// ```json
    /// {
    ///     context: [
    ///         "https://www.w3.org/2018/credentials/v1",
    ///         "https://www.w3.org/2018/credentials/examples/v1"
    ///     ]
    /// }
    /// ```
    /// To overwrite default `context`s use method `setContext()` can be used.
    /// ```javascript
    /// import { VCredential } from 'caelum_vcdm';
    /// let vc = new VCredential("my_id")
    ///     .setContext("new_context");
    /// ```
    ///
    #[wasm_bindgen(js_name = setContext)]
    pub fn set_context(self, c: String) -> Self {
        let mut vc: VerifiableCredential = self.ctx.into_serde().unwrap();
        vc.context = Vec::new();
        vc.context.push(c);
        VCredential { ctx: vc.to_json() }
    }

    /// # Set Type
    /// By default `type` will be set to
    /// ```json
    /// {
    ///     type: [
    ///         "VerifiableCredential",
    ///         "PersonalInformation"
    ///     ]
    /// }
    /// ```
    /// To overwrite default `type`s use method `setType()` can be used.
    /// ```javascript
    /// import { VCredential } from 'caelum_vcdm';
    /// let vc = new VCredential("my_id")
    ///     .setType("new_type");
    /// ```
    ///
    #[wasm_bindgen(js_name = setType)]
    pub fn set_type(self, t: String) -> Self {
        let mut vc: VerifiableCredential = self.ctx.into_serde().unwrap();
        vc.credential_type = Vec::new();
        vc.credential_type.push(t);
        VCredential { ctx: vc.to_json() }
    }

    /// # Set Id
    /// By default `id` will be set by the constructor.
    /// To overwrite default `id` use method `setId()` can be used.
    /// ```javascript
    /// import { VCredential } from 'caelum_vcdm';
    /// let vc = new VCredential("my_id")
    ///     .setId("my_id");
    /// ```
    #[wasm_bindgen(js_name = setId)]
    pub fn set_id(self, id: String) -> Self {
        let mut vc: VerifiableCredential = self.ctx.into_serde().unwrap();
        vc.id = id;
        VCredential { ctx: vc.to_json() }
    }

    /// # Set Issuer
    /// By default `issuer` will be set to empty.
    /// To overwrite default `issuer` use method `setIssuer()` can be used.
    /// ```javascript
    /// import { VCredential } from 'caelum_vcdm';
    /// let vc = new VCredential("my_id")
    ///     .setIssuer("my_issuer");
    /// ```
    ///
    #[wasm_bindgen(js_name = setIssuer)]
    pub fn set_issuer(self, i: String) -> Self {
        let mut vc: VerifiableCredential = self.ctx.into_serde().unwrap();
        vc.issuer = i;
        VCredential { ctx: vc.to_json() }
    }

    /// # Set Claim
    /// By default `claims` will be set to empty array.
    /// To overwrite default `claims` use method `setClaims()` can be used.
    /// ```javascript
    /// import { VCredential } from 'caelum_vcdm';
    /// ```
    ///
    #[wasm_bindgen(js_name = setClaim)]
    pub fn set_claims(self, c: JsValue) -> Self {
        let mut vc: VerifiableCredential = self.ctx.into_serde().unwrap();
        vc.claims = Vec::new();
        vc.claims.push(Claim::from_json(c));
        VCredential { ctx: vc.to_json() }
    }

    /// # Set Proof
    /// By default `proof` will be set to empty.
    /// To overwrite default `proof` use method `setProof()` can be used.
    /// ```javascript
    /// import { VCredential } from 'caelum_vcdm';
    /// let vc = new VCredential("my_id")
    ///     .setProof({
    ///        "type": "RsaSignature2018",
    ///        "created": "2018-06-17T10:03:48Z",
    ///        "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
    ///        "signatureValue": "pY9...Cky6Ed = "
    ///    });
    /// ```
    ///
    #[wasm_bindgen(js_name = setProof)]
    pub fn set_proof(self, p: JsValue) -> Self {
        let mut vc: VerifiableCredential = self.ctx.into_serde().unwrap();
        vc.proofs = Vec::new();
        vc.proofs.push(Proof::from_json(p));
        VCredential { ctx: vc.to_json() }
    }

    /// # Set NonRevocationProof
    /// By default `nonRevocationProof` will be set to empty.
    /// To overwrite default `nonRevocationProof` use method `setNonRevocationProof()` can be used.
    /// ```javascript
    /// import { VCredential } from 'caelum_vcdm';
    /// ```
    ///
    #[wasm_bindgen(js_name = setNonRevocationProof)]
    pub fn set_non_revocation_proof(self, p: JsValue) -> Self {
        let mut vc: VerifiableCredential = self.ctx.into_serde().unwrap();
        vc.non_revocation_proof = Vec::new();
        vc.non_revocation_proof
            .push(NonRevocationProof::from_json(p));
        VCredential { ctx: vc.to_json() }
    }

    // ADDERS
    /// # Add context
    /// By default `context` will be set to
    /// ```json
    /// {
    ///     context: [
    ///         "https://www.w3.org/2018/credentials/v1",
    ///         "https://www.w3.org/2018/credentials/examples/v1"
    ///     ]
    /// }
    /// ```
    /// To **add** to the default `context` array use method `addContext()` can be used.
    /// ```javascript
    /// import { VCredential } from 'caelum_vcdm';
    /// let vc = new VCredential("my_id")
    ///     .addContext("another_context");
    /// ```
    ///
    #[wasm_bindgen(js_name = addContext)]
    pub fn add_context(self, c: String) -> Self {
        let mut vc: VerifiableCredential = self.ctx.into_serde().unwrap();
        vc.context.push(c);
        VCredential { ctx: vc.to_json() }
    }

    /// # Add Claim
    /// To **add** to the default `claims` array use method `addClaim()` can be used.
    /// ```javascript
    /// import { VCredential } from 'caelum_vcdm';
    /// ```
    ///
    #[wasm_bindgen(js_name = addClaim)]
    pub fn add_claim(self, c: JsValue) -> Self {
        let mut vc: VerifiableCredential = self.ctx.into_serde().unwrap();
        vc.claims.push(Claim::from_json(c));
        VCredential { ctx: vc.to_json() }
    }

    /// # Add Proof
    /// By default `proof` will be set to an empty `Vec::new()`.
    /// To overwrite default `proof` use method `addProof()` can be used.
    /// ```javascript
    /// import { VCredential } from 'caelum_vcdm';
    /// let vc = new VCredential("my_id")
    ///     .addProof({
    ///        "type": "RsaSignature2018",
    ///        "created": "2018-06-17T10:03:48Z",
    ///        "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
    ///        "signatureValue": "pY9...Cky6Ed = "
    ///    });
    /// ```
    ///
    #[wasm_bindgen(js_name = addProof)]
    pub fn add_proof(self, p: JsValue) -> Self {
        let mut vc: VerifiableCredential = self.ctx.into_serde().unwrap();
        vc.proofs.push(Proof::from_json(p));
        VCredential { ctx: vc.to_json() }
    }

    /// # Add NonRevocationProof
    /// By default `monRevocationProof` will be set to an empty `Vec::new()`.
    /// To overwrite default `nonRevocationProof` use method `addNonRevocationProof()` can be used.
    /// ```javascript
    /// import { VCredential } from 'caelum_vcdm';
    /// ```
    ///
    #[wasm_bindgen(js_name = addNonRevocationProof)]
    pub fn add_non_revocation_proof(self, p: JsValue) -> Self {
        let mut vc: VerifiableCredential = self.ctx.into_serde().unwrap();
        vc.non_revocation_proof
            .push(NonRevocationProof::from_json(p));
        VCredential { ctx: vc.to_json() }
    }

    /// # Add Type
    /// By default `type` will be set to:
    /// ```json
    /// {
    ///     type: [
    ///         "VerifiableCredential",
    ///         "PersonalInformation"
    ///     ]
    /// }
    /// ```
    /// Once a property `type` is set, one may want to add to the array. To do so use method
    /// `addType()`.
    /// ```javascript
    /// import { VCredential } from 'caelum_vcdm';
    /// let vc = new VCredential("my_id")
    ///     .addType("new_type");
    /// ```
    ///
    #[wasm_bindgen(js_name = addType)]
    pub fn add_type(self, t: String) -> Self {
        let mut vc: VerifiableCredential = self.ctx.into_serde().unwrap();
        vc.credential_type.push(t);
        VCredential { ctx: vc.to_json() }
    }

    // GETTERS
    /// # Get Context
    /// ```javascript
    /// import { VCredential } from 'caelum_vcdm';
    /// let vc = new VCredential("my_id")
    /// console.log(vc.getContext())
    /// ```
    /// The code above will print in console the following:
    /// ```json
    /// {
    ///     context: [
    ///         "https://www.w3.org/2018/credentials/v1",
    ///         "https://www.w3.org/2018/credentials/examples/v1"
    ///     ]
    /// }
    /// ```
    ///
    #[wasm_bindgen(js_name = getContext)]
    pub fn get_context(&self) -> JsValue {
        let vc: VerifiableCredential = self.ctx.into_serde().unwrap();
        JsValue::from_serde(&vc.context).unwrap()
    }

    /// # Get Type
    /// ```javascript
    /// import { VCredential } from 'caelum_vcdm';
    /// let vc = new VCredential("my_id")
    ///     .setType({id: "my_id", type: "my_type"})
    /// console.log(vc.getType())
    /// ```
    /// The code above will print in console the following:
    /// ```json
    /// {
    ///     id: "my_id",
    ///     type: "my_type"
    /// }
    /// ```
    ///
    #[wasm_bindgen(js_name = getType)]
    pub fn get_type(&self) -> JsValue {
        let vc: VerifiableCredential = self.ctx.into_serde().unwrap();
        JsValue::from_serde(&vc.credential_type).unwrap()
    }

    /// # Get Id
    /// ```javascript
    /// import { VCredential } from 'caelum_vcdm';
    /// let vc = new VCredential("my_id");
    /// console.log(vc.getId())
    /// ```
    /// The code above will print in console the following:
    /// ```json
    /// id: "my_id"
    /// ```
    ///
    #[wasm_bindgen(js_name = getId)]
    pub fn get_id(&self) -> JsValue {
        let vc: VerifiableCredential = self.ctx.into_serde().unwrap();
        JsValue::from_serde(&vc.id).unwrap()
    }

    /// # Get Issuer
    /// ```javascript
    /// import { VCredential } from 'caelum_vcdm';
    /// let vc = new VCredential("my_id")
    ///     .setIssuanceDate("2018-06-17T10:03:48Z"})
    /// console.log(vc.getIssuanceDate())
    /// ```
    /// The code above will print in console the following:
    /// ```json
    /// "2018-06-17T10:03:48Z"
    /// ```
    ///
    #[wasm_bindgen(js_name = getIssuer)]
    pub fn get_issuer(&self) -> JsValue {
        let vc: VerifiableCredential = self.ctx.into_serde().unwrap();
        JsValue::from_serde(&vc.issuer).unwrap()
    }

    /// # Get Claims
    /// ```javascript
    /// import { VCredential } from 'caelum_vcdm';
    /// let vc = new VCredential("my_id")
    ///     .setIssuanceDate("2018-06-17T10:03:48Z"})
    /// console.log(vc.getIssuanceDate())
    /// ```
    /// The code above will print in console the following:
    /// ```json
    /// "2018-06-17T10:03:48Z"
    /// ```
    ///
    #[wasm_bindgen(js_name = getClaims)]
    pub fn get_claims(&self) -> JsValue {
        let vc: VerifiableCredential = self.ctx.into_serde().unwrap();
        JsValue::from_serde(&vc.claims).unwrap()
    }

    /// # Get Proof
    /// ```javascript
    /// import { VCredential } from 'caelum_vcdm';
    /// let vc = new VCredential("my_id")
    ///     .setProof({
    ///        "type": "RsaSignature2018",
    ///        "created": "2018-06-17T10:03:48Z",
    ///        "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
    ///        "signatureValue": "pY9...Cky6Ed = "
    ///     })
    /// console.log(vc.getProof())
    /// ```
    /// The code above will print in console the following:
    /// ```json
    /// [{
    ///     "type": "RsaSignature2018",
    ///     "created": "2018-06-17T10:03:48Z",
    ///     "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
    ///     "signatureValue": "pY9...Cky6Ed = "
    /// }]
    /// ```
    ///
    #[wasm_bindgen(js_name = getProof)]
    pub fn get_proof(&self) -> JsValue {
        let vc: VerifiableCredential = self.ctx.into_serde().unwrap();
        JsValue::from_serde(&vc.proofs).unwrap()
    }

    /// # Get NonRevocationProof
    /// ```javascript
    /// import { VCredential } from 'caelum_vcdm';
    /// ```json
    /// [{
    ///     "type": "RsaSignature2018",
    ///     "created": "2018-06-17T10:03:48Z",
    ///     "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
    ///     "signatureValue": "pY9...Cky6Ed = "
    /// }]
    /// ```
    ///
    #[wasm_bindgen(js_name = getNonRevocationProof)]
    pub fn get_non_revocation_proof(&self) -> JsValue {
        let vc: VerifiableCredential = self.ctx.into_serde().unwrap();
        JsValue::from_serde(&vc.non_revocation_proof).unwrap()
    }

    /// # Jsonify Verifiable Credential
    /// `toJSON` method will output the current `VCredential` as a `JsValue` (json object).
    #[wasm_bindgen(js_name = toJSON)]
    pub fn to_json(&self) -> JsValue {
        self.ctx.clone()
    }
}

///
/// # WebAssembly gateway for Verifiable Presentation
///
#[wasm_bindgen]
pub struct VPresentation {
    ctx: JsValue,
}

#[wasm_bindgen]
impl VPresentation {
    // CONSTRUCTORS
    /// # WebAssembly Constructor
    /// `VPresentation`'s constructor will create an empty `VPresentation` object. An empty object
    /// is not a valid `VPresentation`.
    /// ```javascript
    /// import { VPresentation } from 'caelum_vcdm';
    /// let vp = new VPresentation("my_id");
    /// ```
    #[wasm_bindgen(constructor)]
    pub fn new(id: String) -> Self {
        let vp = VerifiablePresentation::new(id);
        VPresentation { ctx: vp.to_json() }
    }

    /// # WebAssembly Constructor `formJSON`
    /// `VPresentation`'s constructor will create a `VPresentation` object from input. Input must
    /// be a JSON object with all properties defined. If no value is wanted for certain property,
    /// must be empty. This is also true for sub properties (properties of properties).
    /// ```javascript
    /// import { VPresentation } from 'caelum_vcdm';
    /// let vp_json = {
    ///    "@context": [
    ///        "https://www.w3.org/2018/credentials/v1",
    ///        "https://www.w3.org/2018/credentials/examples/v1"
    ///    ],
    ///    "id": "did:example:ebfeb1276e12ec21f712ebc6f1c",
    ///    "type": [
    ///        "VerifiableCredential",
    ///        "PersonalInformation"
    ///    ],
    ///    "verifiableCredential": [
    ///        {
    ///            "@context": [
    ///                "https://www.w3.org/2018/credentials/v1",
    ///                "https://www.w3.org/2018/credentials/examples/v1"
    ///            ],
    ///            "id": "http://example.com/credentials/4643",
    ///            "type": [
    ///                "VerifiableCredential",
    ///                "PersonalInformation"
    ///            ],
    ///            "issuer": "did:example:ebfeb1276e12ec21f712ebc6f1c",
    ///            "issuanceDate": "2010-01-01T19:73:24Z",
    ///            "credentialStatus": {
    ///                "id": "http://example.com/credentialTypes",
    ///                "type": "PersonalInformation",
    ///                "currentStatus": "available",
    ///                "statusReason": "self-declared"
    ///            },
    ///            "credentialSubject": [{
    ///                "type": "did:example:abfab3f512ebc6c1c22de17ec77",
    ///                "name": "Mr John Doe",
    ///                "mnumber": "77373737373A",
    ///                "address": "10 Some Street, Anytown, ThisLocal, Country X",
    ///                "birthDate": "1982-02-02-00T00:00Z"
    ///            }],
    ///            "proof": {
    ///                "type": "RsaSignature2018",
    ///                "created": "2018-06-17T10:03:48Z",
    ///                "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
    ///                "signatureValue": "pY9...Cky6Ed = "
    ///            }
    ///        }
    ///    ],
    ///    "proof": {
    ///        "type": "RsaSignature2018",
    ///        "created": "2018-06-17T10:03:48Z",
    ///        "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
    ///        "signatureValue": "pY9...Cky6Ed = "
    ///    }
    /// };
    /// let ver_pres = VPresentation.fromJSON(vp_json);
    /// ```
    #[wasm_bindgen(js_name = fromJSON)]
    pub fn from_json(json: JsValue) -> Self {
        VPresentation { ctx: json }
    }

    // SETTERS
    /// # Set context
    /// By default `context` will be set to
    /// ```json
    /// {
    ///     context: [
    ///         "https://www.w3.org/2018/credentials/v1",
    ///         "https://www.w3.org/2018/credentials/examples/v1"
    ///     ]
    /// }
    /// ```
    /// To overwrite default `context`s use method `setContext()` can be used.
    /// ```javascript
    /// import { VPresentation } from 'caelum_vcdm';
    /// let vp = new VPresentation("my_id")
    ///     .setContext("new_context");
    /// ```
    ///
    #[wasm_bindgen(js_name = setContext)]
    pub fn set_context(self, c: String) -> Self {
        let mut vp: VerifiablePresentation = self.ctx.into_serde().unwrap();
        vp.context = Vec::new();
        vp.context.push(c);
        VPresentation { ctx: vp.to_json() }
    }

    /// # Set Id
    /// By default `id` will be set by the constructor.
    /// To overwrite default `id` use method `setId()` can be used.
    /// ```javascript
    /// import { VPresentation } from 'caelum_vcdm';
    /// let vp = new VPresentation("my_id")
    ///     .setId("my_id");
    /// ```
    ///
    #[wasm_bindgen(js_name = setId)]
    pub fn set_id(self, id: String) -> Self {
        let mut vp: VerifiablePresentation = self.ctx.into_serde().unwrap();
        vp.id = id;
        VPresentation { ctx: vp.to_json() }
    }

    /// # Set Type
    /// By default `type` will be set to
    /// ```json
    /// {
    ///     type: [
    ///         "VerifiableCredential",
    ///         "PersonalInformation"
    ///     ]
    /// }
    /// ```
    /// To overwrite default `type`s use method `setType()` can be used.
    /// ```javascript
    /// import { VPresentation } from 'caelum_vcdm';
    /// let vp = new VPresentation("my_id")
    ///     .setType("new_type");
    /// ```
    ///
    #[wasm_bindgen(js_name = setType)]
    pub fn set_type(self, t: String) -> Self {
        let mut vp: VerifiablePresentation = self.ctx.into_serde().unwrap();
        vp.presentation_type = Vec::new();
        vp.presentation_type.push(t);
        VPresentation { ctx: vp.to_json() }
    }

    /// # Set Verifiable Credential
    /// By default `verifiableCredential` will be an empty vector `Vec::new()` representing
    /// `Vec<VerifiableCredential>. A array of json objects representing a `VCredential`.
    /// ```javascript
    /// import { VPresentation } from 'caelum_vcdm';
    /// let vp = new VPresentation("my_id")
    ///     // Variable `vc` represents a `VCredential` object
    ///     .addVerifiableCredential(vp.toJSON())
    /// ```
    /// Equivalent to
    /// ```javascript
    /// import { VPresentation } from 'caelum_vcdm';
    /// let vp = new VPresentation("my_id")
    ///     .addVerifiableCredential({
    ///         "@context": [
    ///             "https://www.w3.org/2018/credentials/v1",
    ///             "https://www.w3.org/2018/credentials/examples/v1"
    ///         ],
    ///         "id": "http://example.com/credentials/4643",
    ///         "type": [
    ///             "VerifiableCredential",
    ///             "PersonalInformation"
    ///         ],
    ///         "issuer": "did:example:ebfeb1276e12ec21f712ebc6f1c",
    ///         "issuanceDate": "2010-01-01T19:73:24Z",
    ///         "credentialStatus": {
    ///             "id": "http://example.com/credentialTypes",
    ///             "type": "PersonalInformation",
    ///             "currentStatus": "available",
    ///             "statusReason": "self-declared"
    ///         },
    ///         "credentialSubject": [{
    ///             "type": "did:example:abfab3f512ebc6c1c22de17ec77",
    ///             "name": "Mr John Doe",
    ///             "mnumber": "77373737373A",
    ///             "address": "10 Some Street, Anytown, ThisLocal, Country X",
    ///             "birthDate": "1982-02-02-00T00:00Z"
    ///         }],
    ///         "proof": {
    ///             "type": "RsaSignature2018",
    ///             "created": "2018-06-17T10:03:48Z",
    ///             "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
    ///             "signatureValue": "pY9...Cky6Ed = "
    ///         }
    ///     });
    /// ```
    ///
    #[wasm_bindgen(js_name = setVerifiableCredential)]
    pub fn set_verifiable_credential(self, json: JsValue) -> Self {
        let mut vp: VerifiablePresentation = self.ctx.into_serde().unwrap();
        vp.verifiable_credential = Vec::new();
        vp.verifiable_credential.push(json.into_serde().unwrap());
        VPresentation { ctx: vp.to_json() }
    }

    /// # Set Proof
    /// /// By default `proof` will be set to empty.
    /// To overwrite default `proof` use method `setProof()` can be used.
    /// ```javascript
    /// import { VPresentation } from 'caelum_vcdm';
    /// let vp = new VPresentation("my_id")
    ///     .setProof({
    ///         "type": "RsaSignature2018",
    ///         "created": "2018-06-17T10:03:48Z",
    ///         "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
    ///         "signatureValue": "pY9...Cky6Ed = "
    ///     });
    /// ```
    ///
    #[wasm_bindgen(js_name = setProof)]
    pub fn set_proof(self, p: JsValue) -> Self {
        let mut vp: VerifiablePresentation = self.ctx.into_serde().unwrap();
        vp.proofs = Vec::new();
        vp.proofs.push(Proof::from_json(p));
        VPresentation { ctx: vp.to_json() }
    }

    // ADDERS
    /// # Add context
    /// By default `context` will be set to
    /// ```json
    /// {
    ///     context: [
    ///         "https://www.w3.org/2018/credentials/v1",
    ///         "https://www.w3.org/2018/credentials/examples/v1"
    ///     ]
    /// }
    /// ```
    /// To **add** to the default `context` array use method `addContext()` can be used.
    /// ```javascript
    /// import { VPresentation } from 'caelum_vcdm';
    /// let vp = new VPresentation("my_id")
    ///     .addContext("another_context");
    /// ```
    ///
    #[wasm_bindgen(js_name = addContext)]
    pub fn add_context(self, c: String) -> Self {
        let mut vp: VerifiablePresentation = self.ctx.into_serde().unwrap();
        vp.context.push(c);
        VPresentation { ctx: vp.to_json() }
    }

    /// # Add Type
    /// By default `type` will be set to:
    /// ```json
    /// {
    ///     type: [
    ///         "VerifiableCredential",
    ///         "PersonalInformation"
    ///     ]
    /// }
    /// ```
    /// Once a property `type` is set, one may want to add to the array. To do so use method
    /// `addType()`.
    /// ```javascript
    /// import { VPresentation } from 'caelum_vcdm';
    /// let vp = new VPresentation("my_id")
    ///     .addType("new_type");
    /// ```
    ///
    #[wasm_bindgen(js_name = addType)]
    pub fn add_type(self, t: String) -> Self {
        let mut vp: VerifiablePresentation = self.ctx.into_serde().unwrap();
        vp.presentation_type.push(t);
        VPresentation { ctx: vp.to_json() }
    }

    /// # Add Verifiable Credential
    /// By default `verifiableCredential` create an empty vector. In order to add/push more
    /// `verifiableCredential`s to the array use method `addVerifiableCredential()`.
    /// ```javascript
    /// import { VPresentation } from 'caelum_vcdm';
    /// let vp = new VPresentation("my_id")
    ///     .addVerifiableCredential({
    ///         "@context": [
    ///             "https://www.w3.org/2018/credentials/v1",
    ///             "https://www.w3.org/2018/credentials/examples/v1"
    ///         ],
    ///         "id": "http://example.com/credentials/4643",
    ///         "type": [
    ///             "VerifiableCredential",
    ///             "PersonalInformation"
    ///         ],
    ///         "issuer": "did:example:ebfeb1276e12ec21f712ebc6f1c",
    ///         "issuanceDate": "2010-01-01T19:73:24Z",
    ///         "credentialStatus": {
    ///             "id": "http://example.com/credentialTypes",
    ///             "type": "PersonalInformation",
    ///             "currentStatus": "available",
    ///             "statusReason": "self-declared"
    ///         },
    ///         "credentialSubject": [{
    ///             "type": "did:example:abfab3f512ebc6c1c22de17ec77",
    ///             "name": "Mr John Doe",
    ///             "mnumber": "77373737373A",
    ///             "address": "10 Some Street, Anytown, ThisLocal, Country X",
    ///             "birthDate": "1982-02-02-00T00:00Z"
    ///         }],
    ///         "proof": {
    ///             "type": "RsaSignature2018",
    ///             "created": "2018-06-17T10:03:48Z",
    ///             "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
    ///             "signatureValue": "pY9...Cky6Ed = "
    ///         }
    ///     });
    /// ```
    ///
    #[wasm_bindgen(js_name = addVerifiableCredential)]
    pub fn add_verifiable_credential(self, vc: JsValue) -> Self {
        let mut vp: VerifiablePresentation = self.ctx.into_serde().unwrap();
        vp.verifiable_credential.push(vc.into_serde().unwrap());
        VPresentation { ctx: vp.to_json() }
    }

    // GETTERS
    /// # Get Context
    /// ```javascript
    /// import { VPresentation } from 'caelum_vcdm';
    /// let vp = new VPresentation("my_id")
    /// console.log(vp.getContext())
    /// ```
    /// The code above will print in console the following:
    /// ```json
    /// {
    ///     context: [
    ///         "https://www.w3.org/2018/credentials/v1",
    ///         "https://www.w3.org/2018/credentials/examples/v1"
    ///     ]
    /// }
    /// ```
    ///
    #[wasm_bindgen(js_name = getContext)]
    pub fn get_context(&self) -> JsValue {
        let vp: VerifiablePresentation = self.ctx.into_serde().unwrap();
        JsValue::from_serde(&vp.context).unwrap()
    }

    /// # Get Verifiable Credential
    /// ```javascript
    /// import { VPresentation } from 'caelum_vcdm';
    /// let vp = new VPresentation("my_id")
    ///     addVerifiableCredential(serde_json::from_str(r#"{
    ///         "@context": [
    ///             "https://www.w3.org/2018/credentials/v1",
    ///             "https://www.w3.org/2018/credentials/examples/v1"
    ///         ],
    ///         "id": "http://example.com/credentials/4643",
    ///         "type": [
    ///             "VerifiableCredential",
    ///             "PersonalInformation"
    ///         ],
    ///         "issuer": "did:example:ebfeb1276e12ec21f712ebc6f1c",
    ///         "issuanceDate": "2010-01-01T19:73:24Z",
    ///         "credentialStatus": {
    ///             "id": "http://example.com/credentialTypes",
    ///             "type": "PersonalInformation",
    ///             "currentStatus": "available",
    ///             "statusReason": "self-declared"
    ///         },
    ///         "credentialSubject": [{
    ///             "type": "did:example:abfab3f512ebc6c1c22de17ec77",
    ///             "name": "Mr John Doe",
    ///             "mnumber": "77373737373A",
    ///             "address": "10 Some Street, Anytown, ThisLocal, Country X",
    ///             "birthDate": "1982-02-02-00T00:00Z"
    ///         }],
    ///         "proof": [{
    ///             "type": "RsaSignature2018",
    ///             "created": "2018-06-17T10:03:48Z",
    ///             "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
    ///             "signatureValue": "pY9...Cky6Ed = "
    ///         }]
    ///     }"#).unwrap());
    /// console.log(vp.getVerifiableCredential());
    /// ```
    /// The code above will print in console the following:
    /// ```json
    /// {
    ///         "@context": [
    ///             "https://www.w3.org/2018/credentials/v1",
    ///             "https://www.w3.org/2018/credentials/examples/v1"
    ///         ],
    ///         "id": "http://example.com/credentials/4643",
    ///         "type": [
    ///             "VerifiableCredential",
    ///             "PersonalInformation"
    ///         ],
    ///         "issuer": "did:example:ebfeb1276e12ec21f712ebc6f1c",
    ///         "issuanceDate": "2010-01-01T19:73:24Z",
    ///         "credentialStatus": {
    ///             "id": "http://example.com/credentialTypes",
    ///             "type": "PersonalInformation",
    ///             "currentStatus": "available",
    ///             "statusReason": "self-declared"
    ///         },
    ///         "credentialSubject": [{
    ///             "type": "did:example:abfab3f512ebc6c1c22de17ec77",
    ///             "name": "Mr John Doe",
    ///             "mnumber": "77373737373A",
    ///             "address": "10 Some Street, Anytown, ThisLocal, Country X",
    ///             "birthDate": "1982-02-02-00T00:00Z"
    ///         }],
    ///         "proof": {
    ///             "type": "RsaSignature2018",
    ///             "created": "2018-06-17T10:03:48Z",
    ///             "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
    ///             "signatureValue": "pY9...Cky6Ed = "
    ///         }
    ///     }
    /// }
    /// ```
    ///
    #[wasm_bindgen(js_name = getVerifiableCredential)]
    pub fn get_verifiable_credential(&self) -> JsValue {
        let vp: VerifiablePresentation = self.ctx.into_serde().unwrap();
        JsValue::from_serde(&vp.verifiable_credential).unwrap()
    }

    /// # Get Type
    /// ```javascript
    /// import { VPresentation } from 'caelum_vcdm';
    /// let vp = new VPresentation("my_id")
    ///     .setType("my_type")
    ///     .addType("my_type2")
    /// console.log(vp.getType())
    /// ```
    /// The code above will print in console the following:
    /// ```json
    /// ["my_type", "my_type2"]
    /// ```
    ///
    #[wasm_bindgen(js_name = getType)]
    pub fn get_type(&self) -> JsValue {
        let vp: VerifiablePresentation = self.ctx.into_serde().unwrap();
        JsValue::from_serde(&vp.presentation_type).unwrap()
    }

    /// # Get Id
    /// ```javascript
    /// import { VPresentation } from 'caelum_vcdm';
    /// let vp = new VPresentation("my_id");
    /// console.log(vp.getId())
    /// ```
    /// The code above will print in console the following:
    /// ```json
    /// id: "my_id"
    /// ```
    ///
    #[wasm_bindgen(js_name = getId)]
    pub fn get_id(&self) -> JsValue {
        let vp: VerifiablePresentation = self.ctx.into_serde().unwrap();
        JsValue::from_serde(&vp.id).unwrap()
    }

    /// # Get Proof
    /// ```javascript
    /// import { VPresentation } from 'caelum_vcdm';
    /// let vp = new VPresentation("my_id")
    ///     .setProof({
    ///        "type": "RsaSignature2018",
    ///        "created": "2018-06-17T10:03:48Z",
    ///        "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
    ///        "signatureValue": "pY9...Cky6Ed = "
    ///     })
    /// console.log(vp.getProof())
    /// ```
    /// The code above will print in console the following:
    /// ```json
    /// {
    ///     "type": "RsaSignature2018",
    ///     "created": "2018-06-17T10:03:48Z",
    ///     "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
    ///     "signatureValue": "pY9...Cky6Ed = "
    /// }
    /// ```
    ///
    #[wasm_bindgen(js_name = getProof)]
    pub fn get_proof(&self) -> JsValue {
        let vp: VerifiablePresentation = self.ctx.into_serde().unwrap();
        JsValue::from_serde(&vp.proofs).unwrap()
    }

    // GENERATOR
    /// # Jsonify Verifiable Presentation
    /// `toJSON` method will output the current `VPresentation` as a `JsValue` (json object).
    ///
    #[wasm_bindgen(js_name = toJSON)]
    pub fn to_json(&self) -> JsValue {
        self.ctx.clone()
    }
}
