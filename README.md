# **V**erifiable **C**redential **D**ata **M**odel [WIP]

This crates implements w3's [Verifiable Credential Data Model](https://www.w3.org/TR/verifiable-claims-data-model/)
specifications.
This means it will create, read, update and delete (a.k.a. CRUD) all properties of a
`Verifiable Credential`and a `Verifiable Presentation`. As well as interacting with other
`Verifiable Credential` and `Verifiable Presentation`.

> A `Verifiable Credential` or a `Verifiable Presentation`  can represent all of the same information that a physical credential represents. The addition of technologies, such as digital signatures, makes verifiable credentials more tamper-evident and more trustworthy than their physical counterparts.
> In the physical world, a credential might consist of:

- Information related to identifying the subject of the credential (for example, a photo, name, or identification number)
- Information related to the issuing authority (for example, a city government, national agency, or certification body)
- Information related to the type of credential this is (for example, a Dutch passport, an American driving license, or a health insurance card)
- Information related to specific attributes or properties being asserted by the issuing authority about the subject (for example, nationality, the classes of vehicle entitled to drive, or date of birth)
- Evidence related to how the credential was derived
- Information related to constraints on the credential (for example, expiration date, or terms of use).

We want to help developers create these scenarios and making it easy to interact with other following the same specifications.

## Roles
Each of the following [roles](https://www.w3.org/TR/vc-use-cases/) denotes an entity (person or organization) which holds a unique Decentralized Identifier (DID) stored in a blockchain along with a [public key](https://en.wikipedia.org/wiki/Public-key_cryptography), which together can be used to secure communication and verify continuity of identity between all parties.

* The **Subject** is the entity to which the information pertains. The **Holder** is the entity which is holding that information.  For the purposes of Lorena, Subject and Holder are treated as the same.

* The **Issuer** is the entity which validates (either manually or automatically) a piece of information about the Subject, and provides a digital signature (signed with the public key associated with the Issuer's DID) that attests to this validation.

* The **Verifier** is the entity which wants to know something about the Subject.  The Verifier needs to authenticate information about the Subject, and it trusts the Issuer's judgement on that information.

The entities playing these roles all have equal standing: each one has a DID, a (secret) private key and its associated public key which is published with the DID on the blockchain, and each can hold Verifiable Credentials (VCs).  The roles can shift, and can be a peer-to-peer relationship or hierarchical as appropriate to needs.

## Components
### Services
![Component Communications](https://gitlab.com/caelum-tech/lorena/lorena-sdk/raw/master/public/component-communications.png "Component Communications")

The [Lorena Identity Playground](https://gitlab.com/caelum-tech/Lorena/lorena-playground) is a [React](https://reactjs.org/) [single-page application](https://en.wikipedia.org/wiki/Single-page_application) which implements a prototype [identity container](identity-container.md) which allows a **Subject (Holder)** to create a DID, create credentials and ask an *Issuer* to issue a VC, and send a VC to a *Verifier*.

[Hoverfloat](https://gitlab.com/caelum-tech/Lorena/lorena-demo-hoverfloat) is a [demonstration website](https://hoverfloat.com) for a fictional transportation service. It is written in [React](https://reactjs.org/) as a [single-page application](https://en.wikipedia.org/wiki/Single-page_application) with no server back-end. It acts as a **Verifier** which takes VCs sent by a *Subject* and makes sure they were issued by a trusted *Issuer*.

[Lorena Matrix Daemon](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-daemon) is a Node JS back-end service which acts as an **Issuer** of VCs upon request by **Subjects**.  It implements an email verification service and coordinates communication between anonymous guest accounts through Matrix.

[Lorena Substrate](https://gitlab.com/caelum-tech/Lorena/lorena-substrate) is a [Substrate](https://www.parity.io/substrate/)-based blockchain for publishing DIDs and their associated public keys.

[Matrix](https://matrix.org/) is decentralized communication service used for secure communicate between the different entities and services.


## Implementations
- [Rust](https://crates.io/crates/caelum-vcdm)
- [Javascript](https://www.npmjs.com/package/@caelum-tech/caelum-vcdm)

## How to use
- [Rust](https://gitlab.com/caelum-tech/caelum-vcdm/blob/master/Intro-rust.md)
- [Javascript](https://gitlab.com/caelum-tech/caelum-vcdm/blob/master/Intro-js.md)

## Roadmap

- [ X ] Basic CRUD implementation on properties.
- [ X ] Basic CRUD implementation on Optional properties.
- [ X ] Verifying basic cryptographic proofs.
- [ ] Implementation of Zero-Knowledge proof using Zenroom.
- [ ] Connecting to `Verifiable Data Registry`.
- [ ] Validation for necessary properties.
- [ ] Implementation of Roles (`Issuer`, `Holder`, `Verifier`).

## Contributing

Please, contribute to `vcdm`! The more the better! Feel free to to open an issue and/or contacting directly with the owner for any request or suggestion.

## Acknowledgment

This library is created to satisfy w3's specifications.

## Code of conduct

This Code of Conduct is adapted from the [Contributor Covenant][homepage], version 1.4, available at [http://contributor-covenant.org/version/1/4][version]

[homepage]: http://contributor-covenant.org
[version]: http://contributor-covenant.org/version/1/4/

## License

This project is distributed under the terms of both the Apache License (Version 2.0) and the MIT license, specified in [LICENSE-APACHE](LICENSE-APACHE.txt) and [LICENSE-MIT](LICENSE-MIT.txt) respectively.

## Related repositories
* [Lorena Identity Playground](https://gitlab.com/caelum-tech/Lorena/lorena-playground)
* [Demo website: Hoverfloat](https://gitlab.com/caelum-tech/Lorena/lorena-demo-hoverfloat)
* [Lorena Substrate](https://gitlab.com/caelum-tech/Lorena/lorena-substrate)
* [Lorena Matrix Client](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-client)
* [Lorena Matrix Daemon](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-daemon)
* [Lorena Matrix Helpers](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-helpers)
* [caelum-diddoc](https://gitlab.com/caelum-tech/caelum-diddoc)