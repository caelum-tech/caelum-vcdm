## Installation

```bash
yarn add @caelum-tech/caelum-vcdm
```

Now the library is installed for javascript. In order to be able to use a wasm library in javascript follow the following steps:

#### 1. Create in root a file called `config-overrides.js` with the following code in it:

```javascript
const path = require('path');

module.exports = function override(config, env) {
    const wasmExtensionRegExp = /\.wasm$/;

    config.resolve.extensions.push('.wasm');

    config.module.rules.forEach(rule => {
        (rule.oneOf || []).forEach(oneOf => {
            if (oneOf.loader && oneOf.loader.indexOf('file-loader') >= 0) {
                // make file-loader ignore WASM files
                oneOf.exclude.push(wasmExtensionRegExp);
            }
        });
    });

    // add a dedicated loader for WASM
    config.module.rules.push({
        test: wasmExtensionRegExp,
        include: path.resolve(__dirname, 'src'),
        use: [{ loader: require.resolve('wasm-loader'), options: {} }]
    });

    return config;
};
```

#### 2. Install `wasm-loader`

```

yarn add wasm-loader

```

#### 3. Load Wasm library

##### Method A: CommonJs default Nodejs Sync require mechanism:
```
const vcdm = require('@caelum-tech/caelum-vcdm-nodejs')

let vc
try {
  vc = new vcdm.VClaim()      
} catch(err) {
    console.error(`Unexpected error in loadWasm. [Message: ${err.message}]`)
    return undefined
}
console.log(vc.toJSON())

console.log("Good Bye")
```

##### Method B: ES6 Syntax Async Import:

Add the following line in package.json:
```
  "type:": "module"
```
and add/change the start script to:
```
  "start": "node --experimental-modules index.js"
```

```
const main = async () => {
    let vc
    let vcdm = (await import('@caelum-tech/caelum-vcdm-nodejs')).default
    console.log(vcdm)
    try {
      vc = new vcdm.VClaim()      
    } catch(err) {
        console.error(`Unexpected error in loadWasm. [Message: ${err.message}]`)
        return undefined
    }
    console.log(vc.toJSON())
}

main().then(()=>console.log("Good Bye"))
```

## Getting started

```javascript
// Constructing a Claim
let claim = new vcdm.VClaim()
    .setCredentialStatus({id: "my_id", type: "EmailCredential"})
    .setCredentialType(["EmailCredential", "VerifiableCredential"])
    .setCredentialSubject({email: "foo@bar.com"})
    .setId("my_id")
    .setIssuanceDate("2018-06-17T10:03:48Z")
    .setIssuer("did:example:issuer");

console.log(claim.toJSON());

// Constructing a Verifiable Credential
let vc = new vcdm.VCredential("my_id")
    // Setters overwrite values of property
    .setIssuer("Issuer")
    .setClaim(claim.toJSON())
    // Setting another credential totally different and defined by user
    .setProof({
        "type": "RsaSignature2018",
        "created": "2018-06-17T10:03:48Z",
        "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
        "signatureValue": "pY9...Cky6Ed = "
    })
    // Adders push values to the vector properties
    .addContext("added context")
    .addType("added type");

console.log(vc.toJSON());

// Constructing a Verifiable Presentation
let vp = new vcdm.VPresentation("my_id")
    .addContext("added Context")
    .addType("added type")
    // Add the created Verifiable Credential
    .addVerifiableCredential(vc.toJSON())
    .setProof({
        "type": "RsaSignature2018",
        "created": "2018-06-17T10:03:48Z",
        "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
        "signatureValue": "pY9...Cky6Ed = "
    });

console.log(vp.toJSON());
```

or equivalently...

```javascript
let vp_json = {
    "@context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://www.w3.org/2018/credentials/examples/v1",
        "added Context"
    ],
    "id": "did:example:ebfeb1276e12ec21f712ebc6f1c",
    "type": [
        "VerifiableCredential",
        "PersonalInformation",
        "added type"
    ],
    "verifiableCredential": [
        {
             "@context": [
                 "https://www.w3.org/2018/credentials/v1",
                 "https://www.w3.org/2018/credentials/examples/v1"
             ],
             "type": ["VerifiableCredential"],
             "id": "my_id",
             "issuer": "Issuer",
             "claims": [{
                "id": "my_id",
                "credentialStatus": {
                    "id": "my_id",
                    "type": "EmailCredential"
                },
                "credentialSubject": [
                    {"email": "foo@bar.com"}
                ],
                "issuer": "did:example:issuer",
                "issuanceDate": "2018-06-17T10:03:48Z",
                "type": ["EmailCredential", "VerifiableCredential"]
             }],
             "proof": [{
                "type": "RsaSignature2018",
                "created": "2018-06-17T10:03:48Z",
                "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
                "signatureValue": "pY9...Cky6Ed = "
             }],
             "nonRevocationProof": [{
                 "type": "",
                 "created": "",
                 "verificationMethod": "",
                 "signatureValue": ""
             }]
         }
    ],
    "proof": {
        "type": "RsaSignature2018",
        "created": "2018-06-17T10:03:48Z",
        "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
        "signatureValue": "pY9...Cky6Ed = "
    }
};

let vp = vcdm.VPresentation.fromJSON(vp_json);

console.log(vp.toJSON());
```

### Signing

#### Linked Data

```javascript
// Signature from claim
let claim = new vcdm.VClaim()
    .setCredentialStatus({id: "my_id", type: "EmailCredential"})
    .setCredentialType(["EmailCredential", "VerifiableCredential"])
    .setCredentialSubject({email: "foo@bar.com"})
    .setId("my_id")
    .setIssuanceDate("2018-06-17T10:03:48Z")
    .setIssuer("did:example:issuer");
const keys = new Uint8Array(64);
const signature = claim.sign(keys)
console.log(signature)

// Proof from claim (signature is created automatically)
const proof = vcdm.VProof.fromClaimAndKeys(claim.toJSON(), keys)

// Verify signature from proof & claim
console.log(
    vcdm.verifyClaim(claim.toJSON(), proof.toJSON())
)

// Verify signature from claim
console.log(
    claim.verify(proof.toJSON())
)
```

## Publishing this library
To update this library on NPM:
* [Install the prerequisites](Intro-rust.md#prerequisites)
* [Publish the new Crate](Intro-rust.md#publishing-this-library) of the same version on Crates.io
* [Install npm](https://www.npmjs.com/get-npm)
* `cargo install wasm-pack`
* `wasm-pack build`
* Modify `pkg/package.json` as follows:
 * Change the `name` from `caelum-vcdm` to `@caelum-tech/caelum-vcdm`
 * Add the `snippets` directory to the `files` array.
* `npm publish pkg/ --dry-run` and make sure it's okay
* `npm publish pkg/`
