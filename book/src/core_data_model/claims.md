# Claims

*Claims* are done by an *entity* when they are willing to proof something about a *subject*.
*Claims* can be easily represented with knowledge-graphs as follows:

![](https://www.w3.org/TR/vc-data-model/diagrams/claim.svg)

This graph is an abstraction between an *entity* and a *subject*. We can also combine *claims* to
create a more complex claims. In order to create a more trustable *claim* the more complex the 
knowledge-graph should be.

In the following example we can see how *Pat* is "alumni of" *Example University*. As well as
*Pat* "knows" *Sam*, who has a "job title", being that title *Professor*.

![](https://www.w3.org/TR/vc-data-model/diagrams/claim-extended.svg)

