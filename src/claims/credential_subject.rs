use serde::{Deserialize, Serialize};

use linked_hash_map::LinkedHashMap;
use wasm_bindgen::prelude::*;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct CredSubject {
    #[serde(
        flatten,
        rename(serialize = "credentialSubject", deserialize = "credentialSubject"),
        //serialize_with = "ordered_map"
    )]
    pub cred_subj: LinkedHashMap<String, String>,
}

// fn ordered_map<S>(value: &HashMap<String, String>, serializer: S) -> Result<S::Ok, S::Error>
// where
//     S: Serializer,
// {
//     let ordered: BTreeMap<_, _> = value.iter().collect();
//     ordered.serialize(serializer)
// }

impl CredSubject {
    pub fn from_json(json: JsValue) -> Self {
        json.into_serde().unwrap()
    }

    pub fn new(cs: LinkedHashMap<String, String>) -> Self {
        CredSubject { cred_subj: cs }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_ordered_hashmap() {
        let c: CredSubject = serde_json::from_str(
            r#"{ 
            "a":"asd",
            "b": "asd",
            "d": "cvb",
            "c": "gfd"
        }"#,
        )
        .unwrap();
        assert_eq!(
            serde_json::to_string(&c).unwrap(),
            r#"{"a":"asd","b":"asd","d":"cvb","c":"gfd"}"#
        );
    }
}
