## Prerequisites

- Clang >= 8.0
clear_on_drop library needs Clang >= 8.0, otherwise clang won't recognize the wasm32-unknown-unknown triple defined by this architecture.

    - Install clang 8.0 in Ubuntu/Debian:
    ```bash
    sudo apt-install clang-8
    ```

    Configure update-alternatives to work aside with multiple clang versions (Sets Clang 8 as default version):
    ```bash
    sudo update-alternatives --install /usr/bin/clang++ clang++ /usr/bin/clang++-8 100
    sudo update-alternatives --install /usr/bin/clang clang /usr/bin/clang-8 100
    ```

## Installation

Add gitlab repository to `Cargo.toml`:

```bash
[dependencies]
caelum-vcdm = "*"
...
```


## Getting started

A Create, sign and verify example written in Rust [here](examples/create_sign_verify.rs)

```rust
let vc_json_str = r#"{
    "@context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://www.w3.org/2018/credentials/examples/v1"
    ],
    "type": ["VerifiableCredential"],
    "id": "",
    "issuer": "",
    "claims": [{
        "id": "",
        "credentialStatus": {
            "id": "",
            "type": ""
        },
        "credentialSubject": [],
        "issuer": "",
        "issuanceDate": "",
        "type": ["VerifiableCredential"]
    }],
    "proof": [{
        "type": "",
        "created": "",
        "verificationMethod": "",
        "signatureValue": ""
    }],
    "nonRevocationProof": [{
        "type": "",
        "created": "",
        "verificationMethod": "",
        "signatureValue": ""
    }]
}"#;

let _ver_cred_from_json: VerifiableCredential = serde_json::from_str(vc_json_str).unwrap();
```

Equivalent to:

```rust
let mut ver_cred_vcdm =
            VerifiableCredential::new("http://example.com/credentials/4643".to_string());
ver_cred_vcdm.set_proof(
    serde_json::from_str(r#"{
        "type": "RsaSignature2018",
        "created": "2018-06-17T10:03:48Z",
        "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
        "signatureValue": "pY9...Cky6Ed = "
    }"#).unwrap()
);
ver_cred_vcdm.set_credential_subject(
    serde_json::from_str(r#"{
        "type": "did:example:abfab3f512ebc6c1c22de17ec77",
        "name": "Mr John Doe",
        "mnumber": "77373737373A",
        "address": "10 Some Street, Anytown, ThisLocal, Country X",
        "birthDate": "1982-02-02-00T00:00Z"
    }"#).unwrap()
);
ver_cred_vcdm.set_issuance_date("2010-01-01T19:73:24Z".to_string());
ver_cred_vcdm.set_issuer("did:example:ebfeb1276e12ec21f712ebc6f1c".to_string());
ver_cred_vcdm.set_credential_status(
    serde_json::from_str(r#"{
        "id": "cred_stat_id",
        "type": "cred_stat_credential_subject_type"
    }"#).unwrap(),
);
```

### Signing

There are two types of signing claims, with Linked Data or using Zenroom.

#### Linked Data

```rust
// Signature from claim
ley keys = vec![
        212, 109, 1, 120, 214, 102, 78, 169, 141, 239, 187, 76, 224, 61, 74, 250, 20, 4,
        89, 89, 159, 113, 116, 168, 87, 79, 196, 25, 155, 87, 180, 134, 65, 189, 48, 58, 3,
        235, 0, 194, 39, 238, 5, 140, 57, 222, 169, 56, 62, 89, 169, 66, 225, 253, 97, 6,
        241, 72, 72, 72, 195, 209, 12, 81,
    ];
// Creating Claim
let c: Claim = serde_json::from_str(r#"{"credentialStatus":{"id":"https://lorena.caelumlabs.com/credentials","type":"CaelumEmailCredential","currentStatus":"pending","statusReason":"self-issued"},"type":["VerifiableCredential","CaelumEmailCredential"],"credentialSubject":[{"email":"asd","last":"","type":"CaelumEmailCredential","first":""}],"id":"Hash: 0xa76745d559991b293036244416c6818b15d8debf6960151d4c6abf5b284fbb1b","issuanceDate":"2019-10-16T14:07:41.657Z","issuer":"did:lrn:caelum-email-verifier:ebfeb1276e12ec21f712ebc6f1c#k2"}"#).unwrap();

// Proof from claim (signature is created automatically)
let p: Proof = Proof::from_claim_and_keys(
    &c,
    &keys,
);

// Verify signature from proof & claim
assert_eq!(verify_claim(&c, &p), Ok(()));

// Verify signature from claim
println!("{:#?}",
    c.verify(&p)
)
```

## Publishing this library
* Bump the version number in `Cargo.toml` according to [semver](http://semver.io/) principles.
* Check in your changes and merge them.
* Set up your account on [crates.io](https://crates.io), create a new API key and log in.
* Contact an owner of the [caelum-vcdm](https://crates.io/crates/caelum-vcdm) crate to get publishing rights
* `cargo publish`
* `git tag vX.Y.Z`
* `git push origin vX.Y.Z`
* Publish the [Javascript version](Intro-js.md#publishing-this-library)
