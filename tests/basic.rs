/*
extern crate caelum_vcdm;

use std::collections::HashMap;
use vcdm::cred_subject::CredSubject;
use vcdm::verifiable_credential::VerifiableCredential;
use vcdm::verifiable_presentation::VerifiablePresentation;


#[test]
fn test_verifiable_credential_basic() {
    let mut vc1 = VerifiableCredential::default();
    vc1.id = "My_Id".to_string();
    vc1.credential_subject =(
        serde_json::from_str(
            r#"{"credentialSubject": {
                "type": "did:example:abfab3f512ebc6c1c22de17ec77",
                "name": "Mr John Doe",
                "mnumber": "77373737373A",
                "address": "10 Some Street, Anytown, ThisLocal, Country X",
                "birthDate": "1982-02-02-00T00:00Z"
            }}"#,
        )
        .unwrap(),
    );

    let mut vc2 = VerifiableCredential::new("My_Id".to_string());
    let mut hm = HashMap::new();
    hm.insert("birthDate".to_string(), "1982-02-02-00T00:00Z".to_string());
    hm.insert(
        "address".to_string(),
        "10 Some Street, Anytown, ThisLocal, Country X".to_string(),
    );
    hm.insert("name".to_string(), "Mr John Doe".to_string());
    hm.insert(
        "type".to_string(),
        "did:example:abfab3f512ebc6c1c22de17ec77".to_string(),
    );
    hm.insert("mnumber".to_string(), "77373737373A".to_string());
    vc2.set_credential_subject(CredSubject::new(hm.clone()));

    for (k, _) in &hm {
        assert_eq!(
            &vc1.get_credential_subject()[0].cred_subj.get(k),
            &vc2.get_credential_subject()[0].cred_subj.get(k)
        );
    }
}

#[test]
fn from_str_constructors() {
    let vc_json_str = r#"
        {
            "@context": [
                "https://www.w3.org/2018/credentials/v1",
                "https://www.w3.org/2018/credentials/examples/v1"
            ],
            "id": "http://example.com/credentials/4643",
            "type": [
                "VerifiableCredential",
                "PersonalInformation"
            ],
            "issuer": "did:example:ebfeb1276e12ec21f712ebc6f1c",
            "issuanceDate": "2010-01-01T19:73:24Z",
            "credentialSubject": {
                "type": "did:example:abfab3f512ebc6c1c22de17ec77",
                "name": "Mr John Doe",
                "mnumber": "77373737373A",
                "address": "10 Some Street, Anytown, ThisLocal, Country X",
                "birthDate": "1982-02-02-00T00:00Z"
            },
            "proof": {
                "type": "RsaSignature2018",
                "created": "2018-06-17T10:03:48Z",
                "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
                "signatureValue": "pY9...Cky6Ed = "
            },
            "publicKey": [
                {
                    "id": "did:example:ebfeb1276e12ec21f712ebc6f1c#keys-2",
                    "controller": "did:example:pqrstuvwxyz0987654321",
                    "type": "Ed25519VerificationKey2018",
                    "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
                }
            ],
            "authentication": [
                "did:example:ebfeb1276e12ec21f712ebc6f1c",
                {
                    "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
                    "controller": "did:example:pqrstuvwxyz0987654321",
                    "id": "keys-2",
                    "type": "Ed25519VerificationKey2018"
                }
            ],
            "service": [
                {
                    "id": "openid",
                    "type": "OpenIdConnectVersion1.0Service",
                    "serviceEndpoint": "https://openid.example.com/"
                },
                {
                    "id": "openid",
                    "type": "OpenIdConnectVersion1.0Service",
                    "serviceEndpoint": "https://openid.example.com/"
                }
            ],
            "claims": [
                {
                    "subject": "Pat",
                    "property": "alumniOf",
                    "value": "Example University"
                }
            ]
        }
        "#;
    let _: VerifiableCredential = serde_json::from_str(vc_json_str).unwrap();

    let vp_json_str = r#"{
        "@context": [
            "https://www.w3.org/2018/credentials/v1",
            "https://www.w3.org/2018/credentials/examples/v1"
        ],
        "id": "did:example:ebfeb1276e12ec21f712ebc6f1c",
        "type": [
            "VerifiableCredential",
            "PersonalInformation"
        ],
        "verifiableCredential": [
            {
                "@context": [
                    "https://www.w3.org/2018/credentials/v1",
                    "https://www.w3.org/2018/credentials/examples/v1"
                ],
                "id": "http://example.com/credentials/4643",
                "type": [
                    "VerifiableCredential",
                    "PersonalInformation"
                ],
                "issuer": "did:example:ebfeb1276e12ec21f712ebc6f1c",
                "issuanceDate": "2010-01-01T19:73:24Z",
                "credentialSubject": {
                    "type": "did:example:abfab3f512ebc6c1c22de17ec77",
                    "name": "Mr John Doe",
                    "mnumber": "77373737373A",
                    "address": "10 Some Street, Anytown, ThisLocal, Country X",
                    "birthDate": "1982-02-02-00T00:00Z"
                },
                "proof": {
                    "type": "RsaSignature2018",
                    "created": "2018-06-17T10:03:48Z",
                    "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
                    "signatureValue": "pY9...Cky6Ed = "
                },
                "publicKey": [
                    {
                        "id": "did:example:ebfeb1276e12ec21f712ebc6f1c#keys-2",
                        "controller": "did:example:pqrstuvwxyz0987654321",
                        "type": "Ed25519VerificationKey2018",
                        "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
                    }
                ],
                "authentication": [
                    "did:example:ebfeb1276e12ec21f712ebc6f1c",
                    {
                        "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
                        "controller": "did:example:pqrstuvwxyz0987654321",
                        "id": "keys-2",
                        "type": "Ed25519VerificationKey2018"
                    }
                ],
                "service": [
                    {
                        "id": "openid",
                        "type": "OpenIdConnectVersion1.0Service",
                        "serviceEndpoint": "https://openid.example.com/"
                    },
                    {
                        "id": "openid",
                        "type": "OpenIdConnectVersion1.0Service",
                        "serviceEndpoint": "https://openid.example.com/"
                    }
                ],
                "claims": [
                    {
                        "subject": "Pat",
                        "property": "alumniOf",
                        "value": "Example University"
                    }
                ]
            }
        ],
        "proof": {
            "type": "RsaSignature2018",
            "created": "2018-06-17T10:03:48Z",
            "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
            "signatureValue": "pY9...Cky6Ed = "
        }
        }"#;
    let _: VerifiablePresentation = serde_json::from_str(vp_json_str).unwrap();
}

#[test]
fn mixed_constructor() {
    let vc: VerifiableCredential = serde_json::from_str(
        r#"
        {
            "@context": [
                "https://www.w3.org/2018/credentials/v1",
                "https://www.w3.org/2018/credentials/examples/v1"
            ],
            "id": "http://example.com/credentials/4643",
            "type": [
                "VerifiableCredential",
                "PersonalInformation"
            ],
            "issuer": "did:example:ebfeb1276e12ec21f712ebc6f1c",
            "issuanceDate": "2010-01-01T19:73:24Z",
            "credentialSubject": {
                "type": "did:example:abfab3f512ebc6c1c22de17ec77",
                "name": "Mr John Doe",
                "mnumber": "77373737373A",
                "address": "10 Some Street, Anytown, ThisLocal, Country X",
                "birthDate": "1982-02-02-00T00:00Z"
            },
            "proof": {
                "type": "RsaSignature2018",
                "created": "2018-06-17T10:03:48Z",
                "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
                "signatureValue": "pY9...Cky6Ed = "
            },
            "publicKey": [
                {
                    "id": "did:example:ebfeb1276e12ec21f712ebc6f1c#keys-2",
                    "controller": "did:example:pqrstuvwxyz0987654321",
                    "type": "Ed25519VerificationKey2018",
                    "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
                }
            ],
            "authentication": [
                "did:example:ebfeb1276e12ec21f712ebc6f1c",
                {
                    "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
                    "controller": "did:example:pqrstuvwxyz0987654321",
                    "id": "keys-2",
                    "type": "Ed25519VerificationKey2018"
                }
            ],
            "service": [
                {
                    "id": "openid",
                    "type": "OpenIdConnectVersion1.0Service",
                    "serviceEndpoint": "https://openid.example.com/"
                },
                {
                    "id": "openid",
                    "type": "OpenIdConnectVersion1.0Service",
                    "serviceEndpoint": "https://openid.example.com/"
                }
            ],
            "claims": [
                {
                    "subject": "Pat",
                    "property": "alumniOf",
                    "value": "Example University"
                }
            ]
        }
        "#,
    )
    .unwrap();

    let mut vp = VerifiablePresentation::new("MY_ID".to_string());
    vp.add_verifiable_credential(vc);
}
*/
