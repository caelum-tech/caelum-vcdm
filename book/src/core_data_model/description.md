# **V**erifiable **C**redential **D**ata **M**odel

## Description
`VCDM` crate implements a **V**erifiable **C**redential **D**ata **M**odel following 
[w3c specifications](https://www.w3.org/TR/vc-data-model/). All specification related
content in this book are taken from can be further explored in w3c's documentation.
All quotes are taken directly from the source intentionally. 
In this documentation we will **quote** definitions mentioned before and explain how is it 
implemented and how to use it. This means that every section will have:
 - Definition section 
 - Implementation explanation section
 - Examples of usage
 
 This means it will create, read, update and delete (a.k.a. CRUD) all properties of a
 `Verifiable Credential`and a `Verifiable Presentation`. As well as interacting with and verifying other
 `Verifiable Credential` and `Verifiable Presentation`.

## What is a verifiable Credential
*Verifiable credentials* aim to represent regular physical  credentials' information 
with additional properties related to security, interoperability, and trust.
To do so, new technologies such as cryptographic primitives are used to create *verifiable credentials*. 

An entity (*Holder*) can have *verifiable credentials*, and present them to other entities (*Verifiers*)
in a fast manner in order to proof certain information about themselves.

> A `Verifiable Credential` or a `Verifiable Presentation`  can represent all of the same 
information that a physical credential represents. The addition of technologies, such as 
digital signatures, makes verifiable credentials more tamper-evident and more trustworthy 
than their physical counterparts.

> In the physical world, a credential might consist of:
 - Information related to identifying the subject of the credential (for example, a photo, name, 
or identification number)
 - Information related to the issuing authority (for example, a city government, national 
agency, or certification body)
 - Information related to the type of credential this is (for example, a Dutch passport, an 
American driving license, or a health insurance card)
 - Information related to specific attributes or properties being asserted by the issuing 
authority about the subject (for example, nationality, the classes of vehicle entitled to 
drive, or date of birth)
 - Evidence related to how the credential was derived
 - Information related to constraints on the credential (for example, expiration date, or terms 
of use).

We want to help developers create these scenarios and making it easy to interact with other 
following the same specifications.